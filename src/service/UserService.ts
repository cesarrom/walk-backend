import { EmailUtils } from './../utils/EmailUtils';
import { UserDao } from '../dao/UserDao';
import { Controller, Req, Res, Status, Next, Post, Service, Inject } from "@tsed/common";
import { graphql } from 'graphql'
import { schema } from "../schema";
import express from 'express'
const jwt = require('jwt-simple');

@Service()
export class UserService {
  constructor(@Inject(UserDao) private userDao: UserDao, @Inject(EmailUtils) private emailUtils: EmailUtils) { }
  async login(username: string, password: string, req: express.Request, res: express.Response) {
    return this.userDao
      .login({ username, password })
      .then((user) => {
        let payload = {
          id: user.id,
          username: user.username,
          email: user.email,
        }
        var token = jwt.encode(payload, process.env.JWT_SECRET);
        req.headers["authorization"] = token;
        res.cookie("authorization","Bearer "+token)
        return {
          success: true,
          response: token
        }
      }).catch((error) => {
        return {
          success: false,
          error
        }
      });
  }
  inviteUser(config: {
    targetMail: string;
    targetName: string;
    sourceName: string;
    sourceMail: string;
  }) {
    return this.emailUtils.sendInvitationEmail(config)
  }
  static getDecodeUser(encoded){
    let res = encoded?jwt.decode(encoded,process.env.JWT_SECRET):{}
    return res;
  }
}
