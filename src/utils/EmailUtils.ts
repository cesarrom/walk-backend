import { Mail } from './../components/utils/MailTransports';
import MailTime from 'mail-time';
import nodemailer from 'nodemailer';
import * as Q from "q";
import { Service, Inject } from '@tsed/di';

@Service()
export class EmailUtils {
  constructor(@Inject(Mail) private mail: Mail){}
  sendInvitationEmail({
    targetMail,
    targetName,
    sourceName,
    sourceMail
  }){
  return this.mail.sendMail({
      from: `"${sourceName} 👻" <${sourceMail}>`,
      to: targetMail,
      subject: "✔ We want to invite you to join Hikers", // Subject line
      html: `
      <span>Hello</span>&nbsp;<strong>${targetName}</strong>

      <span>✔ We want to invite you to join</span>&nbsp;<a href="http://itshikers.com/">Hikers</a>!
      `
    }).finally(()=>{
      console.log("email sent to -> "+targetMail)
    });
  }
}