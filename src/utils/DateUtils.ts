import moment from 'moment-timezone';

export class DateApi {
    //private promiseStack: Promise<moment.Moment> = Promise.resolve(null)
    //private static getDateUrl: string = "http://worldtimeapi.org/api/timezone/";
    public static readonly originGTM = "GMT-0300"
    private static timezone: string = 'America/Argentina/Buenos_Aires'
    private static minHourLimit: number = -2;
    private static maxHourLimit: number = 2
    public static async initialize(){
        /*fetch("http://worldtimeapi.org/api/ip").then(res=>res.json()).then((res)=>{
            DateApi.timezone = res.timezone;
            console.log(`
            
            CURRENT TIMEZONE: ${DateApi.timezone}

            `)
        })*/
    }
    static now(): moment.Moment {
        return moment().tz(DateApi.timezone)
    }
    static isInsideHourLimits({
        date,
        referenceDate = moment(),
        upperLimit = DateApi.maxHourLimit,
        lowerLimit= DateApi.minHourLimit
    }: {date:string | Date | moment.Moment,referenceDate:string | Date | moment.Moment,upperLimit:number,lowerLimit:number}){
        date = moment(date)
        referenceDate = moment(referenceDate)
        let diff = DateApi.datesHoursDiff(date,referenceDate);
        return diff<upperLimit&&diff>lowerLimit;
    }
    static from(dateA: string | Date | moment.Moment){
        /*if(isString(dateA)&&dateA.indexOf(DateApi.originGTM)>=0){
            dateA = dateA.replace(DateApi.originGTM,"").trim();
            
        }*/
        let m = moment(dateA)
        return m.tz(DateApi.timezone)
    }
    static homogenizeDates(dates:{a:Date | string,b:Date | string}):{a:moment.Moment;b:moment.Moment} {
        let response = {
            a:moment(dates.a).tz(DateApi.timezone),
            b:moment(dates.b).tz(DateApi.timezone)
        }
        return response;
    }
    static datesHoursDiff(dateA: string | Date | moment.Moment, dateB: string | Date | moment.Moment){
        return moment(dateA).tz(DateApi.timezone).diff(moment(dateB).tz(DateApi.timezone),"hours");
    }

}