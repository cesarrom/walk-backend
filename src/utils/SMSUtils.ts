import _  from 'lodash';
let AWS = require('aws-sdk')

AWS.config.region = process.env.AWS_SES_REGION

AWS.config.update({
	accessKeyId     : process.env.AWS_SES_ACCESS_KEY_ID,
	secretAccessKey : process.env.AWS_SES_SECRET_ACCESS_KEY
})

export const SMSUtils = {
	sendMessage : sendMessage
}

function sendMessage(data){

	let sns = new AWS.SNS()

	//SE DEBE ENLAZAR CADA OWNER Y CADA CONTACTO AL PAIS DE RESIDENCIA PARA AGREGARLE EL COD DE TELF CORRECTO

	let phone = data.phone.replace('+','').replace(/\s/g,'').replace(/-/g,'').replace(/[^\x00-\x7F]/g, '')

	phone = _.trimStart(phone, '0')

	phone = phone.length == 10 ? '+549' + phone : phone.length == 11 ? '+54' + phone : '+' + phone

	let params = {
		Message           : data.message,
		MessageStructure  : 'string',
		MessageAttributes : {
			'AWS.SNS.SMS.SMSType' : {
				DataType    : 'String',
				StringValue : 'Transactional'
			}
		},
		PhoneNumber       : phone
	}

	sns.publish(params, function(error, response) {

		if( error ){

			console.log('***********************************************')
			console.log('ERROR SENDING MESSAGE')
			console.log('error')
			console.log(error)
			console.log('phone')
			console.log(phone)
			console.log('***********************************************')

			return {
				success : false,
				error   : error
			}

		}

		console.log(response)

		return {
			success  : true,
			response : response
		}

	})

}
