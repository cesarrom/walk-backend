import _ from 'lodash';
import * as fs from 'fs';
let Moment = require('moment');

let SUPPORT_MAIL = 'soporte@homecloud.com.ar';

let TEMPLATES = {

  newAuthorizedContact: 'newAuthorizedContact.html',

  newInvitation: 'newInvitation.html',

  acceptedInvitation: 'acceptedInvitation.html',

  newInvitationExtension: 'newInvitationExtension.html',

  acceptedInvitationExtension: 'acceptedInvitationExtension.html',

  newClient: 'newClient.html',

  newOwner: 'newOwner.html',

  newGuard: 'newGuard.html',

  newAdmin: 'newAdmin.html',

  passwordChange: 'passwordChange.html',

  rejectVisit: 'rejectVisit.html',

  invitationPDF: 'invitationPDF.html',

  information: 'information.html',

  newEventInvitation: 'newEventInvitation.html',

  sendMessageToGuests: 'sendMessageToGuests.html',

  notifyGuestRemove: 'notifyGuestRemove.html',

  notifyEventCancelled: 'notifyEventCancelled.html',

  authorizedContactPDF: 'authorizedContactPDF.html',

};

let IMAGES_TEMPLATES = {

  one: '<td> <div style="margin: 10px; border-radius: 50%; background-color: #cccccc"> <img style="background:white;border-radius: 50%;display: inline-block;width: 50px;max-height: 50px;min-height: 50px;background-position: center center;background-size: cover;vertical-align: middle;" src="{__CONTACT_IMAGE__}" /> </div> </td>',

  two: '<td style="width:30%;"> <img style="background:white;border-radius: 50%;width: 90px;max-height: 90px;min-height: 90px;position: relative;left: 5px;" src="{__OTHER_CONTACT_IMAGE__}" /> <img style="border-radius: 50%;width: 60px;max-height: 60px;min-height: 60px;position: relative;left: -25px;top: 10px;" src="{__CONTACT_IMAGE__}" /> </td>'
}

function getWebUrlToShow() {

  let regexString = 'http';
  if (_.includes(process.env.WEB_URL, 'https')) {

    regexString += 's';
  }
  regexString += '://';

  return _.replace(process.env.WEB_URL, new RegExp(regexString, 'g'), '');
}

export const FileTemplates = {

  getFileTemplate: function getFileTemplate(options) {

    let template = fs.readFileSync(__dirname + '/../../../files/' + TEMPLATES[options.id], 'utf8');

    _.each(options.params, function (params) {

      template = _.replace(template, new RegExp(params.pattern, 'g'), params.value);
    });

    if (!_.isNil(options.date)) {

      template = _.replace(template, new RegExp('{__DATE__}', 'g'), (options.date));
    }

    if (!_.isNil(options.imagesParams)) {

      let imagesTemplate = IMAGES_TEMPLATES[(options.imagesParams.length === 1 ? 'one' : 'two')];
      _.each(options.imagesParams, function (params) {

        imagesTemplate = _.replace(imagesTemplate, new RegExp(params.pattern, 'g'), params.value);
      });
      template = _.replace(template, new RegExp('{__IMAGES__}', 'g'), imagesTemplate);
    }

    if (options.setWebLoginUrl) {

      template = _.replace(template, new RegExp('{__WEB_LOGIN_URL__}', 'g'), process.env.WEB_URL + '/login');
      template = _.replace(template, new RegExp('{__WEB_LOGIN_URL_TO_SHOW__}', 'g'), getWebUrlToShow() + '/login');
    }

    template = _.replace(template, new RegExp('{__WEB_URL__}', 'g'), process.env.WEB_URL);
    template = _.replace(template, new RegExp('{__WEB_URL_TO_SHOW__}', 'g'), getWebUrlToShow());
    template = _.replace(template, new RegExp('{__SUPPORT_MAIL__}', 'g'), SUPPORT_MAIL);
    template = _.replace(template, new RegExp('{__WEB_IMAGES_URL__}', 'g'), process.env.WEB_URL + '/images');
    template = _.replace(template, new RegExp('{__SERVER_IMAGES_URL__}', 'g'), _.replace(process.env.EXTERNAL_SERVER_URL, '/parse', '/images'));

    return template;
  }
};

