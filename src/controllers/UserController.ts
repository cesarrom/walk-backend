import { Controller, Post, Req, Res, Inject, Authenticated } from "@tsed/common";
import { UserService } from "../service/UserService";


@Controller("/")
export class UserController {
  constructor(@Inject() private userService: UserService){}
  @Post("/login")
  async login(@Req() req, @Res() res){
    let user = req.body||{}
    return this.userService.login(user.username,user.password,req,res)
  }
  @Post("/invite")
  @Authenticated()
  async inviteUser(@Req() req, @Res() res){
    let config = {
      ...req.body,
      sourceName: req.user.username,
      sourceMail: req.user.email
    }
    this.userService.inviteUser(config)
    .then(()=>console.log("email sent!"))
    return {
      success: true
    }
  }
}