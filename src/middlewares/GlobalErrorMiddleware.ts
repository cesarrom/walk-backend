import { NextFunction as ExpressNext, Request as ExpressRequest, Response as ExpressResponse } from "express";
import { MiddlewareError, Request, Response, Next, Err, IMiddlewareError } from "@tsed/common";

@MiddlewareError()
export class GlobalErrorMiddleware implements IMiddlewareError {

    use(
        @Err() error: any,
        @Request() req: any,
        @Response() res: ExpressResponse,
        @Next() next: ExpressNext
    ): any {
        error = error || new Error()
        console.error(error)
        res.status(error.code || 500)
        let code = error.code || 500
        let description = error.message || error.description
        description = description || error.stack
        res.json({
            success: false,
            error: {
                code,
                description,
                message: description
            }
        })
    }
}