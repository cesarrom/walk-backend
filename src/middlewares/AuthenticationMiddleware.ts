import { OverrideMiddleware, AuthenticatedMiddleware, EndpointInfo, Req, Next, EndpointMetadata, Res, IMiddleware, Inject } from "@tsed/common";
import { Forbidden } from "ts-httpexceptions";
import Express from 'express';
import { UserService } from "../service/UserService";

@OverrideMiddleware(AuthenticatedMiddleware)
export class MyAuthMiddleware implements IMiddleware {
    public use(@Req() request: Express.Request,
        @Next() next: Express.NextFunction) { // next is optional here
        let token = request.header('authorization');
        if (!token) {
            throw new Forbidden("Must Provide a session token")
        }
        request['user'] = UserService.getDecodeUser(token)
        return next(); 
    }
}