// MyFooFactory.ts
import {registerFactory} from "@tsed/common";
import nodemailer from "nodemailer";

export interface ISMTPConfig {
  host: string
  from: string
  port?: string
  auth: {
    user: string
    pass: string
  }
}
export const Transport = nodemailer.createTransport({
  host: process.env.EMAIL_HOST,
  port: Number(process.env.EMAIL_PORT),
  secure: Boolean(process.env.EMAIL_SECURE), // true for 465, false for other ports
  auth: {
    user: process.env.EMAIL_USERNAME, // generated ethereal user
    pass: process.env.EMAIL_PASSWORD// generated ethereal password
  }
})
export type Mail = typeof Transport;
export const Mail = Symbol("Mail");
registerFactory(Mail, Transport);
