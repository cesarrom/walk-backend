

export class HttpError extends Error {
  constructor(public code: number,public message: string){
    super('Error code '+code+' -> '+message)
  }
}