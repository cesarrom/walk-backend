// MyFooFactory.ts
import {registerFactory} from "@tsed/common";
import { v1 as neo4j } from "neo4j-driver";

export const driver = neo4j.driver(
    process.env.NEO4J_URI,
    neo4j.auth.basic(
        process.env.NEO4J_USER,
        process.env.NEO4J_PASSWORD
    )
);
export type Driver = neo4j.Driver;
export const Driver = Symbol("Driver");
registerFactory(Driver, driver);
