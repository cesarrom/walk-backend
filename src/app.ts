import { driver } from './components/db-driver/Driver';
import { schema, resolvers } from './schema';
import { ServerLoader, ServerSettings } from "@tsed/common";
import Path = require("path");
import passport from 'passport'
import { ApolloServer } from 'apollo-server-express'
import { v1 as neo4j } from "neo4j-driver";
import { IsAuthDirective, HasRoleDirective } from './components/directives';
import { UserService } from './service/UserService';
import Neode from 'neode';
import { typeDefs } from './schema'
/*
,
  # Write your query or mutation here
mutation(
  $email: String,
  $password: String,
  $username: String,
  $name: String,
  $lastname: String,
  $address: String,
  $phoneNumber: String,
  $birthDate: String
){
  signUp(
    email: $email,
  password: $password,
  username: $username,
  name: $name,
  lastname: $lastname,
  address: $address,
  phoneNumber: $phoneNumber,
  birthDate: $birthDate
  ),
  login(username: $username, password: $password){
    token
  }
}
*/
const neode = new Neode(process.env.NEO4J_URI, process.env.NEO4J_USER, process.env.NEO4J_PASSWORD);
const server = new ApolloServer({
    schema,
    schemaDirectives: {
        isAuthenticated: IsAuthDirective,
        hasRole: HasRoleDirective,
    },
    context: ({ req }) => {
        //console.log("examining request -> ",req.headers)
        let user
        try{
            user = UserService.getDecodeUser(req.headers["authorization"])
        }catch(ex){
            console.error(ex)
        }
        return {
            driver,
            user,
            neode
        }
    }
});

@ServerSettings({
    port: process.env.PORT || 1377,
    rootDir: Path.resolve(__dirname),
    acceptMimes: ["application/json"],
    mount: {
        "/": "${rootDir}/cloud/controllers/**\/*.js"
    },
    componentsScan: [
        "${rootDir}/cloud/middlewares/**\/*.js",
        "${rootDir}/cloud/components/**\/*.js",
    ],
    logger: {
        disableRoutesSummary: process.env.NODE_ENV !== 'development'
    }
})
export class Server extends ServerLoader {
    public $onMountingMiddlewares(): void | Promise<any> {
        this
            //.use(express.static(__dirname + '/../public'))
            .use(function (req, res, next) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Headers', 'X-Requested-With');
                res.header('Access-Control-Allow-Headers', 'Content-Type');
                res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE');
                res.header('Access-Control-Allow-Credentials', 'true');
                res.header('Access-Control-Allow-Headers', 'x-parse-session-token');
                next();
            });
        const bodyParser = require('body-parser')
        this
            .use(require('cookie-parser')())
            .use(require('compression')({}))
            .use(require('method-override')())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({ extended: true }));
        server.applyMiddleware({
            app: this.expressApp
        })
        return null;
    }
    public $onServerInitError(err) {
        console.error(err);
        throw err;
    }
}
new Server().start();
