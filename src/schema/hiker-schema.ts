export const typeDefs = /* GraphQL */`
type Address {
    id: ID!
    level: Int
    kind: Int
    name: String
    latitude: Float
    longitude: Float
    relatedLocation: [Address] @relation(name:"RelatedAddressLocation",direction:IN)
}
type Role {
    id: ID!
    name: String
    number: Int
    description: String 
}
type Signature {
    id: ID!
    signature: String
    name: String
}
type Hiker {
    id: ID!
    username: String
    email: String
    password: String
    apiKey: String
    state: HikerStateEnum
    roles: [Role] @relation(name: "RelatedHikerRole",direction:BOTH)
    location: Address @relation(name: "RelatedHikerAddress",direction:BOTH)
    details: HikerDetails @relation(name: "RelatedHikerDetails",direction:OUT)
    configuration: HikerConfig @relation(name: "RelatedHikerConfiguration",direction:OUT)
    signature: Signature @relation(name: "RelatedHikersignature",direction:BOTH)
    relatedSubHikes: [SubHike] @relation(name: "RelatedHikerHike",direction:OUT)
    relatedHikers: [Hiker] @relation(name: "RelatedHikerOther",direction:OUT)
    relatedCategories: [Category] @relation(name: "RelatedHikerCategory",direction:OUT)
    relatedHikes: [Hike] @relation(name: "RelatedHikerHiker",direction:OUT)
    relatedTag: [Tag] @relation(name: "RelatedHikerTag",direction:OUT)
}
type HikerConfig {
    id: ID!
    sessionDuration: Float
    allowNotifications: Boolean
    twoFactorAuthentication: String
}
type HikerDetails {
    id: ID!
    name: String
    lastname: String
    image: String
    address: String
    phoneNumber: String
    birthString: String
}
`;
