import { IsAuthDirective, HasRoleDirective, CleanFieldDirective } from './../components/directives/index';
import { EmailUtils } from './../utils/EmailUtils';
import { Driver } from './../components/db-driver/Driver';
import { HttpError } from './../components/http-error/HttpError';
import * as GeneralSchema from './graphql-schema'
import { makeAugmentedSchema } from "neo4j-graphql-js";
import { GraphQLSchema } from 'graphql';
import bcrypt from 'bcrypt';
const jwt = require('jwt-simple');
import Neode from 'neode';
import { Transport } from '../components/utils/MailTransports';
export const typeDefs = `
  ${GeneralSchema.typeDefs}
  `
export const resolvers = {
  Mutation: {
    signUp: async (__, hiker, { neode }: { neode: Neode }) => {
      {
        const statementResult = await neode.cypher('MATCH (n:Hiker {username: {username}}) RETURN n', { username: hiker.username })
        let results = statementResult.records || [];
        //console.log(results.length)
        let userRecord = results.find((user) => {
          let u = user.toObject()
          console.log(u)
          return (hiker.username === u["n"]["properties"]["username"])
        })
        if (userRecord) {
          throw new HttpError(404, "User already exists");
        }
      }
      try {
        hiker['password'] = bcrypt.hashSync(hiker['password'], 12)
        let query = `CREATE (a:Hiker ${((hiker) => {
          let properties = "{"
          let props = []
          for (let key in hiker) {
            props.push(" " + key + ": { " + key + " }")
          }
          properties += props.join(", ") + "}"
          return properties;
        })(hiker)}) RETURN a`
        //console.log(query)
        const statementResult = await neode.cypher(query, hiker)
        let results = statementResult.records;
        let user: any = results[0].toObject()
        delete user.password;
        return true
      } catch (ex) {
        console.log(ex)
        return false
      }
    },
    sendInvitation: async (__, { targetEmail, targetName }, { user }) => {
      console.log("entering here")
      return new EmailUtils(Transport).sendInvitationEmail({
        targetMail: targetEmail,
        targetName: targetName,
        sourceMail: user.email,
        sourceName: user.username
      }).then(() => true)
        .catch(() => false)
    },
    login: async (__, { username, password }, { neode }: { driver: Driver, neode: Neode }) => {
      const statementResult = await neode.cypher('MATCH (n:Hiker {username: {username}}) RETURN n', { username })
      let results = statementResult.records || [];
      //console.log(results.length)
      let userRecord = results.find((user) => {
        let u = user.toObject()
        //console.log(u)
        return bcrypt.compareSync(password, u["n"]["properties"]["password"])
      })
      if (!userRecord) {
        throw new HttpError(404, "User does not exists");
      }
      let user: any = userRecord.toObject()["n"]["properties"];
      user.password = "";
      let payload = {
        id: user.id,
        username: user.username,
        email: user.email,
      }
      var token = jwt.encode(payload, process.env.JWT_SECRET);
      /*req.headers["authorization"] = token;
      res.cookie("authorization","Bearer "+token)*/
      return { token }
    }
  }
}
export const schema: GraphQLSchema = makeAugmentedSchema({
  typeDefs,
  resolvers,
  schemaDirectives: {
    isAuthenticated: IsAuthDirective,
    hasRole: HasRoleDirective,
    cleanField: CleanFieldDirective
  }
});


