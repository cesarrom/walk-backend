
export const typeDefs = /* GraphQL */`
type User @isAuthenticated {
  id: ID!
  name: String
  friends: [User] @relation(name: "FRIENDS", direction: "BOTH")
  reviews: [Review] @relation(name: "WROTE", direction: "OUT")
  avgStars: Float @cypher(statement: "MATCH (this)-[:WROTE]->(r:Review) RETURN toFloat(avg(r.stars))")
  numReviews: Int @cypher(statement: "MATCH (this)-[:WROTE]->(r:Review) RETURN COUNT(r)")
}

type Business @hasRole(roles:[admin]){
  id: ID!
  name: String
  address: String
  city: String
  state: String
  reviews: [Review] @relation(name: "REVIEWS", direction: "IN")
  categories: [Category] @relation(name: "IN_CATEGORY", direction: "OUT")
}
type Category {
    id: ID!
    name: String
    description: String
}

type Classification {
    id: ID!
    name: String
    description: String
    minSuggestedAge: Int
    maxSuggestedAge: Int
}
type Tag {
    id: ID!
    name: String
}
`;
