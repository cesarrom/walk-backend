
export const typeDefs = /* GraphQL */`
directive @isAuthenticated on FIELD_DEFINITION
directive @hasRole(role: String) on FIELD_DEFINITION
directive @cleanField on FIELD_DEFINITION
type Address {
    id: ID!
    level: Int
    kind: Int
    name: String
    latitude: Float
    longitude: Float
    relatedLocation: [Address] @relation(name:"RelatedAddressLocation",direction:BOTH)
}
type Category {
    id: ID!
    name: String
    description: String
}
type Classification {
    id: ID!
    name: String
    description: String
    minSuggestedAge: Int
    maxSuggestedAge: Int
}
# this represents a place in the universe
type Sector {
    id: ID!
    name: String
    owner: Hiker @relation(name:"RelatedSectorHiker",direction:BOTH)
}
# this is a space owned by someone where some knowledge is placed in form of an adventure, where every trip is as important as the knowledge it holds
type HikeGalaxy {
    id: ID!
    name: String
    status: Int
    owner: Hiker @relation(name:"RelatedHikeGalaxyHiker",direction:BOTH)
    constellations: [HikeConstellation] @relation(name:"RelatedHikeGalaxyHikeConstellation",direction:BOTH)
    sector: Sector @relation(name:"RelatedHikeGalaxySector",direction:BOTH)
    tags: [Tag] @relation(name:"RelatedHikeGalaxyTag",direction:BOTH)
}
# this is a discoverable topic which is composed by stars, this represents someone´s adveture to understand a given topic
type HikeConstellation {
    id: ID!
    name: String
    status: Int
    stars: [HikeStar] @relation(name:"RelatedHikeConstellationHikeStar",direction:BOTH)
    sector: String
    tags: [Tag] @relation(name:"RelatedHikeConstellationTag",direction:BOTH)
}
# this is a website someone brough to the platform through a star
type HikeStarOriginator {
    id: ID!
    url: String
    categories: [Category] @relation(name:"RelatedHikeStarOriginatorCategory",direction:BOTH)
    classifications: [Classification] @relation(name:"RelatedHikeStarOriginatorClassification",direction:BOTH)
    childrenStars: [HikeStar] @relation(name:"RelatedHikeStarOriginatorHikeStar",direction:BOTH)
}
# this is a small component based on a website
type HikeStar {
    id: ID!
    status: Int
    name: String
    originator: HikeStarOriginator @relation(name:"RelatedHikeStarOriginatorHikeStar",direction:BOTH)
    latitude: Float
    longitude: Float
    composition: [HikeStarComposition] @relation(name:"RelatedHikeStarHikeStarComposition",direction:BOTH)
    autor: Hiker @relation(name:"RelatedHikeStarHiker",direction:BOTH)
    teleporters: [HikeStar] @relation(name:"RelatedHikeStarOther",direction:BOTH)
}
# these are decorators for a star intended to make understanding of a star or a constellation more clear
type HikeStarComposition {
    id: ID!
    status: Int
    type: Int
    selector: String
    resource: String
    decoratorColor: String
    decoratorIcon: String
}

type Role {
    id: ID!
    name: String
    number: Int
    description: String 
}
type Signature {
    id: ID!
    signature: String
    name: String
}
type Tag {
    id: ID!
    name: String
}
type Hiker {
    id: ID!
    username: String
    email: String
    password: String @cleanField
    name: String
    lastname: String
    image: String
    address: String
    phoneNumber: String
    birthDate: String
    apiKey: String
    state: Int
    roles: [Role] @relation(name:"RelatedHikerRole",direction:BOTH)
    location: Address @relation(name:"RelatedAddressHiker",direction:BOTH)
    configuration: HikerConfig @relation(name:"RelatedHikerHikerConfig",direction:BOTH)
    signature: Signature  @relation(name:"RelatedHikerSignature",direction:BOTH)
    relatedSubHikes: [HikeStar] @relation(name:"RelatedHikerStarHiker",direction:BOTH)
    relatedHikers: [Hiker] @relation(name:"RelatedHikerOther",direction:BOTH)
    relatedCategories: [Category] @relation(name:"RelatedHikerCategory",direction:BOTH)
    relatedHikes: [HikeStarOriginator] @relation(name:"RelatedHikeStarOriginatorHiker",direction:BOTH)
    relatedTag: [Tag] @relation(name:"RelatedHikerTag",direction:BOTH)
}
type HikerConfig {
    id: ID!
    sessionDuration: Float
    allowNotifications: Boolean
    twoFactorAuthentication: String
}
type Session {
    token: String
}
type Mutation {
    login(username: String!, password: String!): Session
    sendInvitation(targetEmail: String!, targetName: String!): Boolean @isAuthenticated
    signUp(email: String,
  password: String,
  username: String,
  name: String,
  lastname: String,
  address: String,
  phoneNumber: String,
  birthDate: String): Boolean
}
type URLShortenen {
    id: ID!
    targetUrl: !String
}
`;
