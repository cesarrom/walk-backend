export const typeDefs = /* GraphQL */`
# this represents who can do what on the instance its associated with
type Permissions {
    id: ID!
    name: String
    provider: Hiker
    editPermissions: Permission
    create: Permission
    read: Permission
    modify: Permission
    delete: Permission
}
# this represents a place in the universe
type Sector {
    id: ID!
    status: Int # Acti
    type: Int # Universe => Galaxy => Constellation => Star
    parent: Sector
    children: [Sector]
    name: String
    owner: Hiker
    permissions: Permissions
}
`;
