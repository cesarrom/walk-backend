export const typeDefs = /* GraphQL */`
# this represents a place in the universe
type Sector {
    id: ID!
    name: String
    owner: Hiker
    permissions: Permissions
}
# this is a space owned by someone where some knowledge is placed in form of an adventure, where every trip is as important as the knowledge it holds
type HikeGalaxy {
    id: ID!
    permissions: Permissions
    name: String
    status: Int
    owner: Hiker
    constellations: [HikeConstellation]
    sector: Sector
    tags: [Tag]
}
# this is a discoverable topic which is composed by stars, this represents someone´s adveture to understand a given topic
type HikeConstellation {
    id: ID!
    name: String
    status: Int
    stars: [HikeStar]
    sector: String
    tags: [Tag]
    permissions: Permissions
}
# this is a website someone brough to the platform through a star
type HikeStarOriginator {
    id: ID!
    url: String
    categories: [Category]
    classifications: [Classification]
    childrenStars: [HikeStar]
}
# this is a small component based on a website
type HikeStar {
    id: ID!
    status: Int
    name: String
    originator: HikeStarOriginator
    latitude: Float
    longitude: Float
    composition: [HikeStarComposition]
    autor: Hiker
    permissions: Permissions
    teleporters: [HikeStarTeleporter]
}
# these are decorators for a star intended to make understanding of a star or a constellation more clear
type HikeStarComposition {
    id: ID!
    status: Int
    type: Int
    selector: String
    resource: String
    decoratorColor: String
    decoratorIcon: String
}
`;
