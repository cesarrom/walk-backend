import { HttpError } from './../components/http-error/HttpError';
import { Driver } from './../components/db-driver/Driver';
import { Service, Inject } from "@tsed/di";
import bcrypt from 'bcrypt';

@Service()
export class UserDao {
  constructor(@Inject(Driver) private driver: Driver) { }
  async login({ username, password }) {
    const session = this.driver.session();
    const statementResult = await session.run(
      'MATCH (n:User {username: $username}) RETURN n',
      { username }
    );
    let results = statementResult.records;
    let userRecord = results.find((user) => {
      return bcrypt.compareSync(password,user.get("password"))
    })
    if(!userRecord){
      throw new HttpError(404,"User does not exists");
    }
    session.close();
    let user: any = userRecord.toObject();
    delete user.password;
    return user
  }
}