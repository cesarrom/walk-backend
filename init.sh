#!/bin/bash
echo "initializing application with $NODE_ENV"
foreEnv=$NODE_ENV
export NODE_ENV=development
echo "saving the value $foreEnv while setting $NODE_ENV"
npm install;
export NODE_ENV=$foreEnv
echo "now that is installed, getting $NODE_ENV back"
case $NODE_ENV in
	development)
		npm run start:dev
		;;
	production)
		npm run start:prod
		;;
	local)
		npm run start:local
		;;
  esac