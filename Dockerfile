FROM node:carbon
RUN mkdir parse
ADD . /hikers
WORKDIR /hikers
RUN npm install pm2 -g
EXPOSE 1337
RUN sed -i 's/\r$//' init.sh
VOLUME /hikers
VOLUME /hikers/dist
VOLUME /hikers/node_modules
CMD [ "/bin/bash","init.sh" ]


